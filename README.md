# VSC_Extensions

extensions:
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/svipas/vsextensions/code-autocomplete/1.2.1/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/monokai/vsextensions/theme-monokai-pro-vscode/1.1.19/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-python/vsextensions/python/2021.5.842923320/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-toolsai/vsextensions/jupyter/2021.3.619093157/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/CoenraadS/vsextensions/bracket-pair-colorizer/1.0.61/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/formulahendry/vsextensions/code-runner/0.11.4/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/KevinRose/vsextensions/vsc-python-indent/1.14.2/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ionutvmi/vsextensions/path-autocomplete/1.17.1/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/streetsidesoftware/vsextensions/code-spell-checker/1.10.2/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/PKief/vsextensions/material-icon-theme/4.7.0/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/cweijan/vsextensions/vscode-office/2.2.0/vspackage


TBD:
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/pranaygp/vsextensions/vscode-css-peek/4.2.0/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/humao/vsextensions/rest-client/0.24.5/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/42Crunch/vsextensions/vscode-openapi/4.5.2/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/GitLab/vsextensions/gitlab-workflow/3.20.1/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/dongli/vsextensions/python-preview/0.0.4/vspackage
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/shardulm94/vsextensions/trailing-spaces/0.3.1/vspackage

- https://marketplace.visualstudio.com/items?itemName=tanhakabir.rest-book
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/tanhakabir/vsextensions/rest-book/1.12.0/vspackage


deprecated:
- sainnhe.sonokai
- https://marketplace.visualstudio.com/_apis/public/gallery/publishers/TabNine/vsextensions/tabnine-vscode/3.4.4/vspackage


Note:
https://marketplace.visualstudio.com/_apis/public/gallery/publishers/shardulm94/vsextensions/trailing-spaces/latest/vspackage will download latest version of extension and name it shardulm94.trailing-spaces-latest.vsix > can be used to automate update of extensions.

